// ==UserScript==
// @match        https://gitlab.com/*
// @include      /^.*(freelo|gitlab).*/
// @name         Timestamp on Activity List
// @namespace    https://gitlab.com/MrSwed/gitlab-tampermonkey-scripts/-/blob/main/timestamp-on-activity.monkey
// @version      0.11
// @author       Hannes Erven, MrSwed
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

      // delay required to have related issues load first
  window.setInterval(main, 10000);

  function main() {
   $("time[datetime],[data-time]").each(function(){var _t = this; $(_t).text(new Date($(_t).attr("datetime")||$(_t).attr("data-time")).toLocaleString()) });
  }

})();
