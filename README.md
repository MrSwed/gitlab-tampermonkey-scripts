# Gitlab-Tampermonkey-Scripts

This repository contains some [Tampermonkey](https://www.tampermonkey.net/) scripts

to customize Gitlab's UI. use [timestamp-on-activity.monkey link](https://gitlab.com/MrSwed/gitlab-tampermonkey-scripts/-/blob/main/timestamp-on-activity.monkey)


(C) [Handig Eekhoorn GmbH](https://www.handig-eekhoorn.at/), [CC BY](https://creativecommons.org/licenses/by/)
